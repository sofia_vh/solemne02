<%-- 
    Document   : respuesta
    Created on : 29-may-2021, 19:58:17
    Author     : sofia
--%>

<%@page import="cl.solemne02.entity.Clientes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Clientes cliente=(Clientes) request.getAttribute("cliente");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style2.css"> 
        <title>registroVisita</title>
    </head>
    <body>
        <form name="form1" action="ClienteController" method="POST">
            <div class="d-flex justify-content-center align-items-center" style="background-image: url(https://wwd.com/wp-content/uploads/2017/11/chanel-nordstrom-01.jpg?w=1024);">
                <div class="body"></div>
                <div class="grad"></div>
                <div class="header">
                    <div>Agendar<span>Visita</span><span></span><span>Tienda</span></div><br>
                </div>
                
                <div class="login">
                    <label for="nombre">Nombre</label>
                    <input type="text" value="<%= cliente.getNombre()%>" name="nombre"><br>
                    <br> 
                    <label for="rut">Rut</label><br>
                    <input type="text" value="<%= cliente.getRut()%>" name="rut"><br>
                    <br> 
                    <label for="email">Email</label>
                    <input type="text" value="<%= cliente.getEmail()%>" name="email"><br>
                    <br> 
                    <label for="comuna">Comuna</label>
                    <input type="text" value="<%= cliente.getComuna()%>"placeholder="Ingrese su comuna" name="comuna"><br>
                    <br> 
                    <label for="visita">Dia de su visita</label> 
                    <input type="text" value="<%= cliente.getVisita()%>"placeholder="Ingrese dia de la semana" name="visita"><br>
                     <br> 
                    <button type="submit" name="action" value="modificar">Modificar Informacion</button>
                    <button type="submit" name="action" value="eliminar">Eliminar</button>

                </div>
            </div>
        </form>
    </body>
</html>